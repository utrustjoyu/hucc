<%@ Page Language="c#" Src="~/dispCore/dispPage/PageSet.aspx.cs" Inherits="PageSet"%>
    <%%>
        <!DOCTYPE html>
        <html lang="zh-Hant-TW">

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
            <meta name="author" content="UTRUST 信諾科技" />
            <meta name="keywords" content="" />
            <meta name="description" content="" />
            <meta property="og:title" content="" />
            <meta property="og:description" content="" />
            <meta property="og:site_name" content="" />
            <meta property="og:type" content="website" />
            <meta property="og:url" content="" />
            <meta property="og:image" content="" />
            <meta property="fb:admins" content="" />
            <title>
                <%=TitleName%> ｜ 綠主張GreenVoice</title>
            <link rel="stylesheet" href="/dispPageBox/hucc/assets/bootstrap/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/dispPageBox/hucc/css/fontawesome-all.css">
            <link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/assets/slick/slick.css" />
            <link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/assets/slick/slick-theme.css" />
            <link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/css/components.css" />
            <link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/css/layout.css" />
            <link rel="stylesheet" href="/dispPageBox/hucc/css/style.css">
            <link rel="stylesheet" href="/dispPageBox/hucc/css/hp_green.css">
            <link rel="stylesheet" href="/dispPageBox/hucc/css/style_green.css">
            <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
            <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b07887c1598cc30"></script>
            <!--Google Analytics Script碼(start) 要放在</head>之前-->
            <%=strGoogleAtsCode%>
                <!--Google Analytics Script碼(End)-->
                <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    
    })(window,document,'script','dataLayer','GTM-M28992F');</script>
    
    <!-- End Google Tag Manager -->

        </head>

        <body class="greenPage">
            <!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M28992F"

    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    <!-- End Google Tag Manager (noscript) -->
                <form id="mForm" runat="server">
            <header>
                <div class="logo">
                    <a href="/">
                        <img src="http://hucc.utrust.com.tw/dispPageBox/hucc/images/greenlogo.svg" alt="綠主張‧Green Style">
                    </a>
                    <span class="icon-bar"></span>
                    <a class="headicon headicon-login" href="#" data-toggle="modal" data-target="#modal-login">
                        <span class="icon-login"></span>
                    </a>
                    <asp:PlaceHolder ID="NAV_TOP_BAR" runat="server" />

                </div>
                <div class="main-nav">
                    <asp:PlaceHolder ID="NAV_SUB" runat="server" />
                </div>
            </header>


            <div class="sidebar">
                <div>
                    <div class="sidebarHead">
                        <span class="icon-bar"></span>
                    </div>
                    <asp:PlaceHolder ID="NAV_MAIN" runat="server" />
                </div>
            </div>


            <div class="wrapper" style="min-height:100vh;">

                <!--PAGINAITON-->
                <div class="container">
                    <!-- breadcrumb -->
                    <asp:PlaceHolder ID="AST_1" runat="server"/>
                </div>

                <div class="container">
                    <section class="article alllist">
                        <h2 class="section_title"><asp:PlaceHolder ID="AST_2" runat="server"/></h2>
                            <asp:PlaceHolder ID="CNT_MAIN" runat="server"/>

                </div><!--container END-->
            </div><!--wrapper END-->



                <footer>
                        <asp:PlaceHolder ID="AST_3" runat="server"/>

                </footer>

                <div class="cover"></div>



                <!--LOGIN MODAL-->
                <asp:PlaceHolder ID="AST_4" runat="server"/>


                <!--JS-->
                <script src="/dispPageBox/hucc/js/jquery-2.2.4.min.js"></script>
                <script src="/dispPageBox/hucc/js/jquery-ui.min.js"></script>
                <script src="/dispPageBox/hucc/js/datepicker-zh-TW.js"></script>
                <script src="/dispPageBox/hucc/assets/slick/slick.min.js"></script>
                <script src="/dispPageBox/hucc/assets/bootstrap/js/bootstrap.min.js"></script>
                <script src="/dispPageBox/hucc/js/script.js"></script>
                <script src="/dispPageBox/hucc/js/script_green.js"></script>
            </form>
        </body>

        </html>