$(document).ready(function(){
//datepicker

fc_datepicker();

function fc_datepicker(){
    $( ".datepicker" ).datepicker({dateFormat: 'yy/mm/dd'});
    $('.datepicker-btn').click(function(){
        $(this).siblings('.datepicker').datepicker('show');
    });
}

//open and close sidebar
    $('.logo .icon-bar').on('click',function(){
        $('.sidebar').addClass('open');
        $('.cover').fadeIn();
        unScroll();
    })
    $('.sidebar .icon-bar').on('click',function(){
        $('.sidebar').removeClass('open');
        $('.cover').fadeOut();
        removeUnScroll();
    })
    $('.cover').on('click',function(){
        $('.sidebar').removeClass('open');
        $(this).fadeOut();
        removeUnScroll();
    })
// nav of sidebar
    $('.sidebar_mainnav').hide();

    $('.pfhead.active').on('click',function(){
        $(this).find('.sidebar_mainnav').slideToggle();
        return false;
    })
    $('.sidebar_mainnav > li > a').on('click',function(){
        $(this).siblings('ul').slideToggle();
        return false;
    })

    
// function
    function unScroll(){
        var top = $(document).scrollTop();
        $(document).on('scroll.unable',function (e) {
            $(document).scrollTop(top);
        })
    }
    function removeUnScroll() {
        $(document).unbind("scroll.unable");
    }

// header icon
    $('.headicon-search').on('click',function(){
        if( $(this).hasClass('open') ){
            $(this).removeClass('open');
            $('.headdrop-search').slideUp();
            $(this).find('span').removeClass('icon-close').addClass('icon-search');

            return false;
        }else{
            $(this).addClass('open');
            $('.headdrop-search').slideDown();
            $(this).find('span').removeClass('icon-search').addClass('icon-close');
            $('.headicon-user').removeClass('open');
            $('.headdrop-user').hide();       
            return false;
        }
   
    })

    $('.headicon-user').on('click',function(){
        if( $(this).hasClass('open') ){
            $(this).removeClass('open');
            $('.headdrop-user').slideUp();
            return false;
        }else{
            $(this).addClass('open');
            $('.headdrop-user').slideDown();
            $('.headicon-search').removeClass('open');
            $('.headdrop-search').hide();             
            return false;
        }
   
    })

  //green_topicshp
  $('.linedown a').on('click',function(){
    if(  $(this).parent('.linedown').siblings('.hp_content').hasClass('open') ){
      $(this).parent('.linedown').siblings('.hp_content').removeClass('open');
      $(this).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
      return false;
    }else{
      $(this).parent('.linedown').siblings('.hp_content').addClass('open');
      $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
      return false;
    }

})

    //modal middle
    function centerModals(){
        $('.modal').each(function(i){
          var $clone = $(this).clone().css('display', 'block').appendTo('body');
          var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
          top = top > 0 ? top : 0;
          $clone.remove();
          $(this).find('.modal-content').css("margin-top", top);
        });
      }
      $('.modal').on('show.bs.modal', centerModals);
      $(window).on('resize', centerModals);

        //slick
        $('.swiper-wrapper').slick({
            arrows: false,
            dots: true
          });
        

});//document



