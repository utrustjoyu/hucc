$(document).ready(function () {


  // carousel

  $('.carousel').slick();



  //
  if ($(window).width() <= 420) {
    $('.hpRecipeSlider , .magSlider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
    });
  } else if ($(window).width() <= 768) {
    $('.hpRecipeSlider , .magSlider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
    });
  } else if ($(window).width() <= 1024) {
    $('.hpRecipeSlider , .magSlider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
    });
  } else {
    $('.hpRecipeSlider , .magSlider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
    });


  };






  //留言板
  if ($('.message').length) {
    $('.btn-reply').on('click', function () {
      var _Top = $(this).parent('.reple_responder').siblings('.media-body').find('.media-reply').offset().top;
      $(this).parent('.reple_responder').siblings('.media-body').find('.media-reply').slideDown();
      $('html,body').animate({
        scrollTop: _Top - $('header').height()
      }, 500);
      return false;
    });
    $('.message_div.reply_Box').find('span').on('click', function () {
      $(this).siblings('div').slideDown();
      $(this).siblings('div').find('textarea').focus();
      $(this).slideUp();
    })
  }










  //greenvoice

  if( $('.otherMag').length ){
    var _MagH = $('.otherMag').offset().top - $(window).height();
    var e = $('.affixContent');
  }




  // scroll


  if ($(window).width() <= 768) {
    $('.affixContent').css('position', 'static');
  } else {
    var _currentPostHeight = $('.currentPost').height();
    $('.currentMag .text-center').height(_currentPostHeight);
    var _currentMagWidth = $('.currentMag').find('.col-sm-6.text-center').width();
    $('.affixContent').width(_currentMagWidth - 30);


    $(window).scroll(function () {
      if ($(this).scrollTop() > _MagH) e.addClass('absobottom');
      else e.removeClass('absobottom');
    });

    $(window).resize(function () {
      var _currentPostHeight = $('.currentPost').height();
      $('.currentMag .text-center').height(_currentPostHeight);
      var _currentMagWidth = $('.currentMag').find('.col-sm-6.text-center').width();
      $('.affixContent').width(_currentMagWidth - 30);

    });
  }








}); //document