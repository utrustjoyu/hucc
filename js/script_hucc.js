$(document).ready(function(){

    $('.carousel').slick();
    
    
    var _W = $(window).width();

        if( _W <= 420 ){
            $('.hpRecipeSlider').slick({
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }else if( _W <= 768 ){
            $('.hpRecipeSlider').slick({
                slidesToShow: 2,
                slidesToScroll: 1
            });
            
        }else if( _W <= 1024 ){
            $('.hpRecipeSlider').slick({
                slidesToShow: 3,
                slidesToScroll: 1
            });
        }else{
            $('.hpRecipeSlider').slick({
                slidesToShow: 4,
                slidesToScroll: 1
            });         
        }
    
 
        $('.prodCarousel').slick({
            arrows: false,
            dots:true
        });   
        
        //accordion
        $('.accordion-head').on('click',function(){
            if ( $(this).hasClass('open') ){
                $(this).removeClass('open');
                $(this).find('span').removeClass('fa-minus').addClass('fa-plus');
                $(this).next('.accordion-content').slideUp(500);
            }else {
                $(this).addClass('open');
                $(this).find('span').removeClass('fa-plus').addClass('fa-minus');
                $(this).next('.accordion-content').slideDown(500);
            }
        });

     // downloadlist filter        
        $('.dlfilter').on('click',function(){  
            var i =  $(this).attr('data-filter');
            $(this).addClass('active').siblings().removeClass('active');

            if( i == 'all' ){
                $('.dlfile').show();
            }else{
                $('.dlfile').hide();
                $('.dlfile-'+i).show();
            }
        })
        
     // product-qa filter        
     $('.qafilter').on('click',function(){  
        var i =  $(this).attr('data-filter');
        $(this).addClass('active').siblings().removeClass('active');

        if( i == 'all' ){
            $('.accordion > div').show();
        }else{
            $('.accordion > div').hide();
            $('.qalist-'+i).show();
        }
    })
    

});//document



