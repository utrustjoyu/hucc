<%@ Page Language="c#" Src="~/dispCore/dispPage/PageSet.aspx.cs" Inherits="PageSet"%>
<%%>
<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="author" content="UTRUST 信諾科技" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
	<meta property="fb:admins" content="" />
	<title><%=TitleName%></title>
	<link rel="stylesheet" href="/dispPageBox/hucc/assets/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/dispPageBox/hucc/css/fontawesome-all.css">
	<link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/assets/slick/slick.css" />
	<link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/assets/slick/slick-theme.css" />
	<link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/css/components.css" />
	<link rel="stylesheet" type="text/css" href="/dispPageBox/hucc/css/layout.css" />
	<link rel="stylesheet" href="/dispPageBox/hucc/css/hp_hucc.css">
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!--Google Analytics Script碼(start) 要放在</head>之前-->
        <%=strGoogleAtsCode%>
		<!--Google Analytics Script碼(End)-->
		<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	
	})(window,document,'script','dataLayer','GTM-M28992F');</script>
	
	<!-- End Google Tag Manager -->
</head>

<body class="huccPage">
	<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M28992F"

	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	
	<!-- End Google Tag Manager (noscript) -->
        <form id="mForm" runat="server">
	<header>
		<div class="logo">
			<a href="/">
				<img src="images/hucclogo.svg" alt="台灣主婦聯盟生活消費合作社">
			</a>
			<span class="icon-bar"></span>
            <asp:PlaceHolder ID="NAV_TOP_BAR" runat="server"/>

		</div>
		<div class="main-nav">
                <asp:PlaceHolder ID="NAV_SUB" runat="server"/>
		</div>
	</header>


	<div class="sidebar">
		<div>
			<div class="sidebarHead">
				<span class="icon-bar"></span>
				
			</div>
            <asp:PlaceHolder ID="NAV_MAIN" runat="server"/>



		</div>
	</div>


	<div class="wrapper" style="min-height:100vh;">
        <!--carousel-->
        <asp:PlaceHolder ID="AST_1" runat="server"/> 

        
		<div class="container quicklink">
			<div class="row">
				<div class="col-xs-4">
					<a href="#">
						<img src="images/quicklink_ecshop.svg" alt="主婦e籃菜E-ORDER">
						<p>主婦e籃菜
							<br>
							<span>E-COMMERCE</span>
							<span class="fas fa-chevron-circle-right"></span>
						</p>
					</a>
				</div>
				<div class="col-xs-4">
					<a href="#">
						<img src="images/quicklink_joinus.svg" alt="入社說明會JOIN US">
						<p>入社說明會
							<br>
							<span>JOIN US</span>
							<span class="fas fa-chevron-circle-right"></span>
						</p>
					</a>
				</div>
				<div class="col-xs-4">
					<a href="#">
						<img src="images/quicklink_location.svg" alt="服務據點LOCATION">
						<p>服務據點
							<br>
							<span>LOCATION</span>
							<span class="fas fa-chevron-circle-right"></span>
						</p>
					</a>
				</div>
			</div>
        </div>
        
        <asp:PlaceHolder ID="AST_2" runat="server"/> 
        
		<div class="container hpProducts">
			<h2>嚴選產品
				<span>PRODUCTS</span>
			</h2>
			<div class="row">
				<div class="col-xs-4">
					<a class="hpProd hpProd01" href="#">
						<div class="hpProdText">
							<p>
							<img src="images/hpPordIcon_egg.svg"><br>
							奶蛋豆‧烘焙</p>
						</div>
					</a>
					<a class="hpProd hpProd02" href="#">
						<div class="hpProdText">
							<p>
							<img src="images/hpPordIcon_fish.svg"><br>
							肉品‧漁產</p>
						</div>
					</a>

				</div>
				<div class="col-xs-4">
					<a class="hpProd hpProd03" href="#">
						<div class="hpProdText">
							<p>
							<img src="images/hpPordIcon_fruit.svg"><br>
							當季蔬果</p>
						</div>
					</a>
				</div>
				<div class="col-xs-4">
					<a class="hpProd hpProd04" href="#">
						<div class="hpProdText">
							<p>
							<img src="images/hpPordIcon_salt.svg"><br>
							調理食材‧點心</p>
						</div>
					</a>
					<a class="hpProd hpProd05" href="#">
						<div class="hpProdText">
							<p>
							<img src="images/hpPordIcon_home.svg"><br>
							當季蔬果</p>
						</div>
					</a>
				</div>
			</div>
		</div>



        <asp:PlaceHolder ID="AST_3" runat="server"/> 
	</div>



	<footer>
            <asp:PlaceHolder ID="AST_4" runat="server"/> 

	</footer>

	<div class="cover"></div>


	<!--LOGIN MODAL-->
    <asp:PlaceHolder ID="AST_5" runat="server"/> 




	<!--JS-->
	<script src="/dispPageBox/hucc/js/jquery-2.2.4.min.js"></script>
	<script src="/dispPageBox/hucc/js/jquery-ui.min.js"></script>
	<script src="/dispPageBox/hucc/js/datepicker-zh-TW.js"></script>
	<script src="/dispPageBox/hucc/assets/slick/slick.min.js"></script>
	<script src="/dispPageBox/hucc/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="/dispPageBox/hucc/js/script.js"></script>
    <script src="/dispPageBox/hucc/js/script_hucc.js"></script>
</form>
</body>

</html>